# about

looking for a solution for 
http://stackoverflow.com/questions/33630929/android-wear-project-with-3-flavors-3-buildtypes-and-2-applicationidsuffixes

check branches for different approaches/situations

# general wear app checklist

general checklist

* phone app module needs a wearApp reference in the dependencies section of grade file pointing to the wear module
* phone app should be built in release (including signing) in order to get the wear app packaged
* in the wear app on the phone you can force app syncing from settings, then it should at least appear in logging as “wearable app … your.package”
* all permissions required by wear app also need to be defined in the phone app
* wear app and phone app require the same packageID for this to work
* unfortunately there seems to be no support for applicationIdSuffix icw wear
* same goes for multiple flavour dimensions, didn’t get that to work so far
* multiple flavors do work, you need to define all flavours in both wear and phone app
* publishNonDefault true has to be present in android section of build grade file of wear module

# other related resources

* http://stackoverflow.com/questions/24685845/android-wear-app-not-installed
* http://tools.android.com/tech-docs/new-build-system
* http://developer.android.com/training/wearables/apps/packaging.html
* http://stackoverflow.com/questions/3997748/how-can-i-create-a-keystore

